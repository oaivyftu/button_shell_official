

(function( $ ) {
  /**
   * START - ONLOAD - JS
   */
  /* ----------------------------------------------- */
  /* ------------- FrontEnd Functions -------------- */
  /* ----------------------------------------------- */

function openMenu() {
  if(!$(".nav-click").length) { return; }

  $(".nav-click").on('click', function(e) {
    // Close menu
    if($(this).hasClass('nav-click--opened')) {
      $('.content-wrap').removeClass('inactivated');
      // $('.content-wrap').addClass('activated');
      $(this).removeClass('nav-click--opened');
      $('.top-menu').removeClass('top-menu--opened');
      $('.home-slider').removeClass('home-slider--scrolled');
      $('.home-slider').slick('slickPlay');
      $('.social-blk').removeClass('social-blk--scrolled');
      $('.logo-stroke').show();
    } else {
      // Open menu
      $('.content-wrap').addClass('inactivated');
      $('.content-wrap').removeClass('activated');
      $(this).addClass('nav-click--opened');
      $('.top-menu').addClass('top-menu--opened');
      $('.home-slider').addClass('home-slider--scrolled');
      $('.home-slider').slick('slickPause');
      $('.social-blk').addClass('social-blk--scrolled');
      $('.logo-stroke').hide();
    }
  });
}

function finishLoading() {
    $('.loading').remove();
    $('.home-slider').addClass('home-slider--opened');
    $('.social-blk').addClass('social-blk--opened');
    initHomeSlider();
    // Active page
    // $('.content-wrap').addClass('activated')
    $('.section').eq(0).addClass('section--play-svg')
    $('.scroll-down').addClass('aniscroll');
    // Reveal sections on scroll
    revealSections();
    // Animate when scroll
    animateScroll()
    // Scroll to top when loading page first time
    setTimeout(function() {
      $(window).scrollTop(0)
    }, 50)
    // Scrolldown
    scrollDown()
    // Scrollup
    scrollUp()
    layoutProds()
}

function initHomeSlider() {
  $('.home-slider').on('init', function (slick) {
    $('.logo-stroke').show();
      $('.home-slider__itm').eq(0).find('.home-slider__ttl').addClass('home-slider__ttl--animate');
    });
  $('.home-slider').slick({
    autoplay: true,
    fade: true,
    autoplaySpeed: 2500,
    speed: 1000,
    arrows: false
  });
  $('.home-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    $('.logo-stroke').hide();
    $('.home-slider__itm').eq(currentSlide).find('.home-slider__ttl').removeClass('home-slider__ttl--animate');
  });
  $('.home-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
    $('.logo-stroke').show();
    $('.home-slider__itm').eq(currentSlide).find('.home-slider__ttl').addClass('home-slider__ttl--animate');
  });
}

function revealSections() {
  ScrollReveal().reveal('.section', { 
    duration: 500, 
    interval: 400, 
    distance: '50px',
    origin: 'bottom'
  });
}

function animateScroll() {
  $(window).on('scroll', function(e) {
    if($(window).scrollTop() > 500) {
      $('.scroll-down').removeClass('aniscroll');
      $('.scroll-down').addClass('fadeOutFlip');
      $('.scroll-up').addClass('activated');
    } else {
      $('.scroll-down').addClass('aniscroll');
      $('.scroll-down').removeClass('fadeOutFlip');
      $('.scroll-up').removeClass('activated');
    }
  })
}

function scrollDown() {
  $('.scroll-down').on('click', function(e) {
    $('html, body').animate({
      scrollTop: 600
    }, 500, 'swing', function() {
      // alert('aa')
    })
  })
}

function scrollUp() {
  $('.scroll-up').on('click', function(e) {
    $('html, body').animate({
      scrollTop: 0
    }, 500, 'swing', function() {
      // alert('aa')
    })
  })
}

function layoutProds() {
  var $grid = $('.section__proj-list').isotope({
    // options
    itemSelector: '.section__proj',
    layoutMode: 'fitRows',
    percentPosition: true,
  });

  // filter items on button click
  $('.section__nav').on( 'click', 'a', function() {
    var filterValue = $(this).attr('data-filter');
    $grid.isotope({ filter: filterValue });

    $(this).closest(".section__nav").find("li").removeClass("section__nav-itm--active");
    $(this).closest("li").addClass("section__nav-itm--active");
  });
}

  /* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
  openMenu();
});
/* OnLoad Window */
var init = function () {
  finishLoading();
};
window.onload = init;

})(jQuery);
