module.exports = function(grunt) {
  grunt.initConfig({
    exec: {
      server: 'node source/server.js'
    },
    pkg: grunt.file.readJSON('package.json'),
    pug: {
      compile: {
        options: {
          pretty: true
        },
        files: [{
            expand: true,
            cwd: 'source/pages/',
            src: ['*.pug'],
            dest: 'public/',
            ext: '.html'
          }
        ]
      }
    },
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'source/assets/css',
          src: ['*.scss'],
          dest: './public/styles',
          ext: '.css'
        }]
      }
    },
    concat: {
      libs: {
        src:  'source/assets/js/libs/jquery-3.3.1.min.js',
        dest: 'public/js/libs.js'
      },
      plugins: {
        src: 'source/assets/js/plugins/*.js',
        dest: 'public/js/plugins.js'
      },
      start: {
        src: 'source/assets/js/start.js',
        dest: 'public/js/start.js'
      }
    },
    copy: {
      ajax: {
        files: [{
            expand: true,
            cwd: 'source/views/ajax/',
            src: '*',
            dest: 'public/ajax/'
          }
        ]
      },
      robots: {
        files: [{
            expand: true,
            cwd: 'source/assets/robots/',
            src: '*',
            dest: 'public'
          }
        ]
      },
      images: {
        files: [{
            expand: true,
            cwd: 'source/assets/images/',
            src: '**/*',
            dest: 'public/images/'
          }
        ]
      },
      uploads: {
        files: [{
            expand: true,
            cwd: 'source/assets/uploads/',
            src: '**/*',
            dest: 'public/uploads/'
          }
        ]
      },
      fonts: {
        files: [{
            expand: true,
            cwd: 'source/assets/fonts/',
            src: '**/*',
            dest: 'public/fonts/'
          }
        ]
      },
      icons: {
        files: [{
          expand: true,
          cwd: 'source/assets/icons/',
          src: '**/*',
          dest: 'public/icons/'
        }]
      }
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      files: ['source/assets/js/plugins/plugin-*.js', 'source/assets/js/*.js']
    },
    watch: {
      options: {
        livereload: true,
      },
      configFiles: {
        files: [ 'Gruntfile.js'],
        options: {
          reload: true
        }
      },
      // livereload: {
      //   // Here we watch the files the sass task will compile to
      //   // These files are sent to the live reload server after sass compiles to them
      //   options: { livereload: true },
      //   files: ['public/**/*'],
      // },
      js: {
        files: ['source/assets/js/plugins/plugin-*.js', 'source/assets/js/*.js'],
        tasks: ['concat']
      },
      pug: {
        files: ['source/pages/**/*.pug', 'source/components/**/*.pug', 'source/share/**/*.pug'],
        tasks: ['pug']
      },
      sass: {
        files: ['source/assets/css/**/*.scss'],
        tasks: ['sass']
      },
      ajax: {
        files: ['source/views/ajax/**/*.*'],
        tasks: ['copy:ajax']
      },
      fonts: {
        files: ['source/assets/fonts/**/*'],
        tasks: ['copy:fonts']
      },
      images: {
        files: ['source/assets/images/**/*'],
        tasks: ['copy:images']
      },
      uploads: {
        files: ['source/assets/uploads/**/*'],
        tasks: ['copy:uploads']
      }
    },
    cssmin: {
      compress: {
        files: [{
            'public/css/style.css': 'public/css/style.css'
          }
        ]
      }
    },
    usemin: {
      // html: ['public/**/*.html'],
      css: ['public/css/**/*.css']
    },
    uglify: {
      options: {
        compress: true,
        beautify: false,
        preserveComments: false
      },
      libs: {
        files: [{
            'public/js/libs.js': 'source/assets/js/libs/jquery-1.9.1.js'
          }
        ]
      },
      plugins: {
        files: [{
            'public/js/plugins.js': 'public/js/plugins.js'
          }
        ]
      }
    },
    clean: {
      build: ['public']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-pug');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-usemin');

  grunt.registerTask('server', ['exec:server']);
  grunt.registerTask('default', ['clean:build', 'concat', 'sass', 'pug', 'copy']);
  grunt.registerTask('build', 'default');
  grunt.registerTask('release', ['build', 'uglify', 'cssmin', 'usemin']);
  grunt.registerTask('deploy', ['build', 'uglify' ]);

};
